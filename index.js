const a = "56";
console.log(a == "56");

/////..............//////

let a2 = 15;
let a3 = "mama";
let a4;
a4 = a2;
a2 = a3;
a3 = a4;
console.log(a2);
console.log(a3);

///////.................///////

const c = "ww" / 1;
console.log(c);

///////.........//////////

const d2 = 3;
const d = (d2 + 2) ** 3 / 6 + 4.5;
console.log(d);
const d3 = d % 5;
console.log(d3);

/////............////////

let z = Number("55");
console.log(z);
console.log(typeof z);

////////////............//////////////

const v = d3 < z;
console.log(v);
